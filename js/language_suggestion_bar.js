(function ($) {
  Drupal.behaviors.language_suggestion_bar = {
    attach: function(context, settings) {
      $('.block-language-suggestion-bar .no-link', context).click(function() {
        $.ajax({
          url: 'language_suggestion_bar/save-session',
          dataType: 'json',
        });

        $(this).parentsUntil('.block-language-suggestion-bar').slideUp("slow");
      });
    }
  };
})(jQuery);
